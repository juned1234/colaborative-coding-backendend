const mongoose = require('mongoose');

// user schema
const contactSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
            required: true,
            max: 32
        },
        email: {
            type: String,
            trim: true,
            required: true,
            lowercase: true
        },
        phone: {
            type: String
        },
        message: {
            type: String
        }
    },
    { timestamps: true }
);




  
  const Contact = mongoose.model('Contact',contactSchema)

  module.exports = Contact;
  