const mongoose = require('mongoose');

// user schema
const taskSchema = new mongoose.Schema(
    {
        user_id: {
            type: String,
            trim: true,
            required: true
        },
        email: {
            type: String,
            trim: true,
            required: true,
            lowercase: true
        },
        task: {
            type: String,
            required:true
        }
    },
    { timestamps: true }
);




  
  const Task = mongoose.model('Task',taskSchema)

  module.exports = Task;
  