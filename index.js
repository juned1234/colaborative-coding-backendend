
const express = require("express");
const http = require("http");
const app = express();
const server = http.createServer(app);
const socket = require("socket.io");
const io = socket(server);

const morgan = require('morgan')
const cors = require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
require('dotenv').config();


const port = process.env.port || 8000

// import routes here
const authRoutes = require('./routes/auth')
const homeRoutes = require('./routes/home')
const contact    = require('./routes/contact')
const task = require('./routes/task')


//mongoodb connection
mongoose.connect(process.env.DbUrl,{ useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true })

.then(()=>console.log("mongodb connected..."))
.catch(err=>console.log("mongodb not connected",err))


//third part and custom middleware goes here
//other middlewares
app.use(morgan('tiny'))
app.use(cors())
app.use(bodyParser.json())


//======socket.io=====//

io.on("connection", socket => {
  socket.emit("your id", socket.id);
  socket.on("send message", body => {
      io.emit("message", body)
  })
})





// routes middleware goes here
app.use(homeRoutes)
app.use('/api',authRoutes)
app.use('/api',contact)
app.use('/api',task)


server.listen(port, () => {
  console.log(` listening at http://localhost:${port}`)
})
