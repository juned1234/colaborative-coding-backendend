const sgMail = require('@sendgrid/mail');

 const Contact = require('../models/contact')

 sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    


exports.contact = (req, res) => {
     console.log(req.body)

     const msg = {
      to: 'jimmywork369@gmail.com',
      from: 'jimmywork369@gmail.com',
      subject: 'Sending with Twilio SendGrid is Fun',
      text: 'and easy to do anywhere, even with Node.js',
      html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    };

     const contact = new Contact({
        name : req.body.name,
        email : req.body.email,
        phone : req.body.mobile,
        message:req.body.message
        
      })
      
      contact.save()
     
      .then(result => {
        console.log("contact saved in db");
        ///====///
        sgMail.send(msg);
      ///====///   
        res.json(result);
      })
      .catch(err => {
        console.log("contact not saved");
        res.send("contact not saved ...",err)
      });



  }