const jwt = require('jsonwebtoken');
const User = require('../models/user')



exports.signup = (req, res) => {
    // console.log(req.body)

    //checking reigsiter email is already in database
     let email = req.body.email
    User.findOne({ email }).exec((err, user) => {
      if (user) {
          return res.status(400).json({
              error: 'Email is taken'
          });
      }
  });
  
    const user = new User({
  name : req.body.name,
  email : req.body.email,
  password : req.body.password
  
})

user.save()
.then(result => {
  console.log(result);
  res.json(result);
})
.catch(err => {
  console.log(err);
  res.send("unable to signup",err)
});

  }


  ///===signin goes here===///

  exports.signin = (req, res) => {
    const { email, password } = req.body;
    // check if user exist
    User.findOne({ email }).exec((err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: 'User with that email does not exist. Please signup'
            });
        }
        // authenticate
        if (!user.authenticate(password)) {
            return res.status(400).json({
                error: 'password do not match'
            });
        }
        // generate a token and send to client
        const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, { expiresIn: '7d' });
        const { _id, name, email, role } = user;

        return res.json({
            token,
            user: { _id, name, email, role }
        });
    });
};